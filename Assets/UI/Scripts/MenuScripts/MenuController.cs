﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace MainMenuSystem
{
    public class MenuController : MonoBehaviour
    {

        #region Menu Settings
        private int menuNumber;
        #endregion

        #region Menu Dialogs
        [Header("Main Menu Components")]
        [SerializeField] private GameObject menuDefaultCanvas;
        [SerializeField] private GameObject LevelSelectorCanvas;
        [SerializeField] private GameObject AchievementsCanvas;
        [SerializeField] private GameObject RankingCanvas;
        [SerializeField] private GameObject GeneralSettingsCanvas;
        [SerializeField] private GameObject UserLoginRegisterCanvas;

        [SerializeField] private GameObject Level2Canvas;
        #endregion

        #region Initialisation - Button Selection & Menu Order
        private void Start()
        {
            menuNumber = 1;
        }
        #endregion

        //MAIN SECTION

        #region Menu Mouse Clicks

        private void ClickSound()
        {
            GetComponent<AudioSource>().Play();
        }

        public void MouseClick(string buttonType)
        {
            if (buttonType == "Options")
            {
                menuDefaultCanvas.SetActive(false);
                GeneralSettingsCanvas.SetActive(true);
                menuNumber = 2;
            }

            if (buttonType == "User")
            {
              menuDefaultCanvas.SetActive(false);
              UserLoginRegisterCanvas.SetActive(true);
              menuNumber = 3;
            }

            if (buttonType == "LevelSelector")
            {
                menuDefaultCanvas.SetActive(false);
                LevelSelectorCanvas.SetActive(true);
                menuNumber = 4;
            }

            if (buttonType == "Achievements")
            {
                menuDefaultCanvas.SetActive(false);
                AchievementsCanvas.SetActive(true);
                menuNumber = 5;
            }

            if (buttonType == "Ranking")
            {
                menuDefaultCanvas.SetActive(false);
                RankingCanvas.SetActive(true);
                menuNumber = 6;
            }

            if (buttonType == "Level2")
            {
                LevelSelectorCanvas.SetActive(false);
                Level2Canvas.SetActive(true);
                menuNumber = 12;
            }
        }
        #endregion

        #region Back to Menus
        public void GoBackToMainMenu()
        {
            menuDefaultCanvas.SetActive(true);
            LevelSelectorCanvas.SetActive(false);
            AchievementsCanvas.SetActive(false);
            RankingCanvas.SetActive(false);
            GeneralSettingsCanvas.SetActive(false);
            UserLoginRegisterCanvas.SetActive(false);
            menuNumber = 1;
        }

        public void GoBackToSelectLevel()
        {
            Level2Canvas.SetActive(false);
            LevelSelectorCanvas.SetActive(true);
            menuNumber = 4;
        }
        #endregion
    }
}
