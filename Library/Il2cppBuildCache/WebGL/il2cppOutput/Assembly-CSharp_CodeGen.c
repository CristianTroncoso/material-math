﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DragDrop::Awake()
extern void DragDrop_Awake_mC73A9D9F329086F4EE63A0DDD784D07C81DC71ED (void);
// 0x00000002 System.Void DragDrop::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnBeginDrag_m04FB0166667DA87A718E017FD6C4F245A3FFC3FF (void);
// 0x00000003 System.Void DragDrop::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnDrag_m2B39A502DFA49FA81908D6BD7FAA410A7EEEBAA4 (void);
// 0x00000004 System.Void DragDrop::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnEndDrag_m112E9CB06836F6734B372D265EC53C44A358786B (void);
// 0x00000005 System.Void DragDrop::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnPointerDown_m5BE74E1ED0D27C6CB8EFB1F5D7A7CE99E879221C (void);
// 0x00000006 System.Void DragDrop::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void DragDrop_OnDrop_m4869E979E1876DC1A89E066E16AB08643C05F648 (void);
// 0x00000007 System.Void DragDrop::.ctor()
extern void DragDrop__ctor_m4AB8772BFCA68BA8F69835AA48046D1ECA6F9B71 (void);
// 0x00000008 System.Void Slot::OnDrop(UnityEngine.EventSystems.PointerEventData)
extern void Slot_OnDrop_mE38C30B596289EC0C387C7C8C9768245E80DF4FE (void);
// 0x00000009 System.Void Slot::.ctor()
extern void Slot__ctor_m2F7B85EC24837E410CD89BEDE6FEB6797EBA801E (void);
// 0x0000000A System.Void MainMenuSystem.MenuController::Start()
extern void MenuController_Start_m4809EB30FE763537557734FFD160FFCB31799265 (void);
// 0x0000000B System.Void MainMenuSystem.MenuController::ClickSound()
extern void MenuController_ClickSound_m565772A505748E1864AFDF6D6787F0635E56191E (void);
// 0x0000000C System.Void MainMenuSystem.MenuController::MouseClick(System.String)
extern void MenuController_MouseClick_mB720C96DCDB1A965D975FB200DDC08DF99FD8EF2 (void);
// 0x0000000D System.Void MainMenuSystem.MenuController::GoBackToMainMenu()
extern void MenuController_GoBackToMainMenu_m5D6D04684D92D034332AD06F943F400D30B2DE83 (void);
// 0x0000000E System.Void MainMenuSystem.MenuController::GoBackToSelectLevel()
extern void MenuController_GoBackToSelectLevel_m29952200C37573E5A8170C407C5632FA7F571E2A (void);
// 0x0000000F System.Void MainMenuSystem.MenuController::.ctor()
extern void MenuController__ctor_m4D634233CA2E0CAAB6DDC33B1AA448440375B5D3 (void);
static Il2CppMethodPointer s_methodPointers[15] = 
{
	DragDrop_Awake_mC73A9D9F329086F4EE63A0DDD784D07C81DC71ED,
	DragDrop_OnBeginDrag_m04FB0166667DA87A718E017FD6C4F245A3FFC3FF,
	DragDrop_OnDrag_m2B39A502DFA49FA81908D6BD7FAA410A7EEEBAA4,
	DragDrop_OnEndDrag_m112E9CB06836F6734B372D265EC53C44A358786B,
	DragDrop_OnPointerDown_m5BE74E1ED0D27C6CB8EFB1F5D7A7CE99E879221C,
	DragDrop_OnDrop_m4869E979E1876DC1A89E066E16AB08643C05F648,
	DragDrop__ctor_m4AB8772BFCA68BA8F69835AA48046D1ECA6F9B71,
	Slot_OnDrop_mE38C30B596289EC0C387C7C8C9768245E80DF4FE,
	Slot__ctor_m2F7B85EC24837E410CD89BEDE6FEB6797EBA801E,
	MenuController_Start_m4809EB30FE763537557734FFD160FFCB31799265,
	MenuController_ClickSound_m565772A505748E1864AFDF6D6787F0635E56191E,
	MenuController_MouseClick_mB720C96DCDB1A965D975FB200DDC08DF99FD8EF2,
	MenuController_GoBackToMainMenu_m5D6D04684D92D034332AD06F943F400D30B2DE83,
	MenuController_GoBackToSelectLevel_m29952200C37573E5A8170C407C5632FA7F571E2A,
	MenuController__ctor_m4D634233CA2E0CAAB6DDC33B1AA448440375B5D3,
};
static const int32_t s_InvokerIndices[15] = 
{
	1099,
	941,
	941,
	941,
	941,
	941,
	1099,
	941,
	1099,
	1099,
	1099,
	941,
	1099,
	1099,
	1099,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	15,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
